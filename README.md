# Lycanite's Menagerie

This project stores all shared 3D models, images, sounds and other assets for the many creature's of Schism based projects such as Lycanite's Mobs and Nephrite Doom.

This should not contain project specific assets such as parts jsons, obj files from model exports for Lycanite's Mobs or sprites for Nephrite Doom as they can be found within the assets of those specific projects.
